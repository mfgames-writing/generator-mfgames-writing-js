# Yeoman Generator for MfGames Writing

This is an semi-opinionated setup for [mfgames-writing-js](https://gitlab.com/mfgames-writing/mfgames-writing-js#README).

## Generating

```
$ sudo npm install -g yo generator-mfgames-writing
$ mkdir name-of-project
$ yo mfgames-writing
... answer questions
$ npm run build
$ npm run test
```

## Nix

This uses [Nix](https://nixos.org/) flakes for reproducible builds and adding hooks for [direnv](https://direnv.net/) to load them directly. Otherwise, you need the following:

-   NodeJS 16.x
-   WeasyPrint
-   PDFtk
-   EPUBcheck
