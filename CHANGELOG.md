# [1.1.0](https://gitlab.com/mfgames-writing/generator-mfgames-writing-js/compare/v1.0.1...v1.1.0) (2021-12-22)


### Bug Fixes

* add required Nix packages for building PDF files ([e9df1b4](https://gitlab.com/mfgames-writing/generator-mfgames-writing-js/commit/e9df1b4d637abbb48335889f9d06c50bdd7b8b57)), closes [#3](https://gitlab.com/mfgames-writing/generator-mfgames-writing-js/issues/3)
* added missing Nix packages references ([e3b75f6](https://gitlab.com/mfgames-writing/generator-mfgames-writing-js/commit/e3b75f6991160c0f25b9c6cc16b568e7b94670e2))
* **generator-mfgames-nix-project:** updating for fixes ([2892bc7](https://gitlab.com/mfgames-writing/generator-mfgames-writing-js/commit/2892bc7a7a270e4c58d6a6c151e3e2eea5e8c769))


### Features

* **epub:** added epubcheck to build ([43707a8](https://gitlab.com/mfgames-writing/generator-mfgames-writing-js/commit/43707a827e186b1fed6fac5c085152098ae881e1)), closes [#1](https://gitlab.com/mfgames-writing/generator-mfgames-writing-js/issues/1)
* **generator-mfgames-nix-project:** pulling in new features ([65a21ef](https://gitlab.com/mfgames-writing/generator-mfgames-writing-js/commit/65a21efe6f82f8c18e90c9264cb305ef2f80e599))
* include the output files on the Gitlab release ([ae8a0df](https://gitlab.com/mfgames-writing/generator-mfgames-writing-js/commit/ae8a0dfc9a7612b0ce8d6f9bb7b0de6dbb4924fb))

## [1.0.1](https://gitlab.com/mfgames-writing/generator-mfgames-writing-js/compare/v1.0.0...v1.0.1) (2021-12-22)


### Bug Fixes

* **generator-mfgames-nix-project:** updating to get release process back in ([b440648](https://gitlab.com/mfgames-writing/generator-mfgames-writing-js/commit/b4406487f2c16d8d8ac3cbcfa31ce98d3572ba16))

# [1.0.0](https://gitlab.com/mfgames-writing/generator-mfgames-writing-js/compare/v0.3.3...v1.0.0) (2021-12-20)


### Features

* **generator-mfgames-nix-project:** bumping version to make package.json pretty ([bf43e06](https://gitlab.com/mfgames-writing/generator-mfgames-writing-js/commit/bf43e06d67857e4096e9d25c612c0b6517c675ec))

# [0.2.0](https://gitlab.com/mfgames-writing/generator-mfgames-writing-js/compare/v0.1.1...v0.2.0) (2021-07-31)


### Features

* added more files to the `.gitignore` ([f3db002](https://gitlab.com/mfgames-writing/generator-mfgames-writing-js/commit/f3db002e3466016a1d8782574644bd01c314627d))
* removed commitizen from default install ([4d9ef96](https://gitlab.com/mfgames-writing/generator-mfgames-writing-js/commit/4d9ef96a9db193657e5f2612b25c85bcdcbde049))
