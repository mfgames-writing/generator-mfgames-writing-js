module.exports = {
    extends: ["@commitlint/config-conventional"],
    branches: ["main"],
    message:
        "chore(release): v${nextRelease.version} [skip ci]\n\n${nextRelease.notes}",
    plugins: [
        [
            "@semantic-release/commit-analyzer",
            {
                preset: "conventionalcommits",
            },
        ],
        "@semantic-release/release-notes-generator",
        "@semantic-release/npm",
        "@semantic-release/changelog",
        "@semantic-release/git",
        "@semantic-release/gitlab",
    ],
};
