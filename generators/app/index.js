const _ = require("lodash");
const Generator = require("yeoman-generator");
const metadata = require("./metadata");
const formats = require("./formats");
const themes = require("./themes");

// Yeoman 5.x.x requires we extend the generator to include the deprecated
// install actions.
_.extend(Generator.prototype, require("yeoman-generator/lib/actions/install"));

// Create the class that represents our application.
module.exports = class extends Generator {
    constructor(args, opts) {
        super(args, opts);
    }

    async prompting() {
        this.answers = await this.prompt(
            [].concat(
                metadata.prompt(this),
                formats.prompt(this),
                themes.prompt(this)
            )
        );

        this.addPdf = this.answers.formats.indexOf("pdf") >= 0;
        this.addEpub = this.answers.formats.indexOf("epub") >= 0;
    }

    async configuring() {
        // Compose with the bulk of the Nix project setup.
        this.composeWith(
            require.resolve("generator-mfgames-nix-project/generators/app"),
            {
                buildCommand: "run-s build:*",
                hasTest: true,
                hasRelease: true,
                gitlabSast: false,
            }
        );

        // Add in the various components.
        await metadata.configure(this);
        await formats.configure(this);
        await themes.configure(this);
    }

    async writing() {
        this.fs.copyTpl(
            this.templatePath("chapter-01.md"),
            this.destinationPath("chapters/chapter-01.md"),
            {
                title: this.answers.title,
            }
        );

        // Write out the .yo-rc.json.
        this.config.save();
    }

    async installing() {
        await themes.install(this);

        this.installDependencies();
    }

    async end() {}
};
