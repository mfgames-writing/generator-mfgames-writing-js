const project = require("generator-mfgames-nix-project");

function prompt(generator) {
    return [
        {
            type: "checkbox",
            name: "formats",
            message: "Which formats do you want to use?",
            choices: [
                { checked: true, name: "PDF", value: "pdf" },
                { checked: true, name: "EPUB", value: "epub" },
                { checked: false, name: "DOCX", value: "docx" },
                { checked: false, name: "HTML", value: "html" },
            ],
            store: true,
        },
        {
            // We don't include publication.config.js because we need to switch
            // parsing libraries. But, this is a future plan so we are keeping
            // the file in the generator.
            type: "list",
            name: "config",
            message: "Which configuration format do you want?",
            choices: ["publication.json", "publication.yaml"],
            default: "publication.json",
        },
    ];
}

async function configure(generator) {
    const pdf = generator.answers.formats.indexOf("pdf") >= 0;
    const epub = generator.answers.formats.indexOf("epub") >= 0;
    const docx = generator.answers.formats.indexOf("docx") >= 0;
    const html = generator.answers.formats.indexOf("html") >= 0;
    const packageManager = generator.answers.packageManager;

    // Add in the EPUB and PDF targets.
    const pkgJson = {
        scripts: {},
    };

    generator.addDependencies({
        "@mfgames-writing/contracts": "^4.1.0",
        "@mfgames-writing/format": "^3.1.1",
        "@mfgames-writing/hyphen-pipeline": "^1.0.2",
    });

    // Add in the individual targets.
    if (epub) {
        pkgJson.scripts["build:epub"] = "mfgames-writing-format build epub";
        (pkgJson.scripts["test:epubcheck"] = "epubcheck *.epub"),
            generator.addDependencies({
                "@mfgames-writing/epub2-format": "^2.1.1",
            });
        project.addNixPackage("pkgs.epubcheck", {
            name: "EPUBcheck",
            url: "https://www.w3.org/publishing/epubcheck/",
        });
        project.addGitlabAsset("*.epub", "EPUB2");
    }

    if (pdf) {
        pkgJson.scripts["build:pdf"] = "mfgames-writing-format build pdf";
        generator.addDependencies({
            "@mfgames-writing/weasyprint-format": "^5.0.3",
        });
        project.addNixPackage("pkgs.python39Full", { name: "Python 3.9" });
        project.addNixPackage("pkgs.python39Packages.weasyprint", {
            name: "WeasyPrint",
            url: "https://weasyprint.org/",
        });
        project.addNixPackage("pkgs.pdftk", {
            name: "PDFtk",
            url: "https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/",
        });
        project.addGitlabAsset("*.pdf", "PDF");
    }

    if (docx) {
        pkgJson.scripts["build:docx"] = "mfgames-writing-format build docx";
        generator.addDependencies({
            "@mfgames-writing/docx-format": "^1.0.2",
        });
        project.addNixPackage("pkgs.pandoc", {
            name: "Pandoc",
            url: "https://pandoc.org/",
        });
        project.addGitlabAsset("*.docx", "DOCX");
    }

    if (html) {
        pkgJson.scripts["build:html"] = "mfgames-writing-format build html";
        generator.addDependencies({
            "@mfgames-writing/html-format": "^1.0.2",
        });
        project.addGitlabAsset("*.html", "HTML");
    }

    // Write out the package to the file system.
    generator.fs.extendJSON(generator.destinationPath("package.json"), pkgJson);

    // Write out the publication file.
    generator.fs.copyTpl(
        generator.templatePath(generator.answers.config),
        generator.destinationPath(generator.answers.config),
        {
            title: generator.answers.title,
            author: generator.answers.author,
            theme:
                generator.answers.theme === "Custom"
                    ? generator.answers.customTheme
                    : generator.answers.theme,
            pdf: pdf,
            epub: epub,
            docx: docx,
            html: html,
        }
    );
}

module.exports = {
    prompt,
    configure,
};
