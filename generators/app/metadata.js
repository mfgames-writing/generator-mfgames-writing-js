const titleCase = require("title-case").titleCase;
const slugify = require("slugify");

function prompt(generator) {
    return [
        {
            type: "input",
            name: "title",
            message: "The title for your novel or story:",
            default: titleCase(generator.appname),
        },
    ];
}

async function configure(generator) {
    // Figure out the package file.
    let pkgJson = {
        title: generator.answers.title,
    };

    // Write out the package to the file system.
    generator.fs.extendJSON(generator.destinationPath("package.json"), pkgJson);
}

module.exports = {
    prompt,
    configure,
};
