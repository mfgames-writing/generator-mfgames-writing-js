function prompt(generator) {
    return [
        {
            type: "list",
            name: "theme",
            message: "The name of the theme package to use:",
            choices: [
                "@mfgames-writing/clean-theme",
                "@mfgames-writing/greekil-theme",
                "Custom",
            ],
            default: "@mfgames-writing/clean-theme",
        },
        {
            when: (answers) => answers.theme === "Custom",
            type: "input",
            name: "customTheme",
            message: "The name of the theme package to use:",
            default: "@mfgames-writing/clean-theme",
        },
    ];
}

async function configure(generator) {}

async function install(generator) {
    // Since the themes are dynamic, we need to add them in a slightly different
    // manner than most dependencies since we don't know the name ahead of time.
    // Since we're going to allow per-format overrides of themes, we need to be
    // able to install multiple themes.
    let themes = {};
    const themeName =
        generator.answers.theme === "Custom"
            ? generator.answers.customTheme
            : generator.answers.theme;

    themes[themeName] = "*";
    generator.addDependencies(themes);
}

module.exports = {
    prompt,
    configure,
    install,
};
