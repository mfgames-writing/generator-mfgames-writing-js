// Set up the metadata from the project.
const metadata = {
    language: "en",
    theme: "<%= theme %>",
    outputDirectory: ".",
    outputFileName: "{{ edition.name }}-{{ edition.version }}.{{ edition.ext }}",
};

// Set up editions.
const editions = {};

<% if (epub) { %>
editions.epub = {
    format: "@mfgames-writing/epub2-format",
    ext: "epub",
};
<% } %>
<% if (pdf) { %>
editions.pdf = {
    format: "@mfgames-writing/weasyprint-format",
    ext: "pdf",
    images: {
        grayscale: true,
        opaque: true,
    },
};
<% } %>
<% if (docx) { %>
editions.docx = {
    format: "@mfgames-writing/docx-format",
    ext: "docx",
};
<% } %>
<% if (html) { %>
editions.html = {
    format: "@mfgames-writing/html-format",
    ext: "html",
};
<% } %>

// Set up common pipelines.
const pipelines = [
    "@mfgames-writing/hyphen-pipeline",
];

// Set up the layout and contents of the file.
const contents = [
    {
        element: "toc",
        linear: false,
        title: "Contents",
        exclude: {
            editions: ["pdf"],
        },
    },
    {
        element: "chapter",
        number: 1,
        directory: "chapters",
        source: "/^chapter-\d+.(md|markdown)$/",
        start: true,
        page: 1,
        pipelines,
    },
]

// Export everything.
module.exports = {
    contents,
    editions,
    content,
}
